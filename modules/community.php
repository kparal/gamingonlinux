<?php
$templating->set_previous('meta_description', 'GamingOnLinux community page', 1);
$templating->set_previous('title', 'Linux gamers community section', 1);

$templating->merge('community');
$templating->block('main');
?>
